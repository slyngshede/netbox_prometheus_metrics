# Netbox Prometheus Metrics

By default Netbox ships with a Prometheus metrics endpoint, which can be enabled to
expose certain internal metrics. These metrics are generally only related to the Django
part of Netbox, not actual Netbox data.

This project adds a few additional metrics, currently only related to reporting, to the
metrics endpoint. It does so by defining and starting an additional collector, which is
automatically picked up by [django-prometheus](https://github.com/korfuri/django-prometheus).

## Getting started

Install this plugin as you would any other Netbox plugin, e.g. using pip, and add it to
the PLUGINS section in your Netbox configuration.

```
PLUGINS = ['netbox_prometheus_metrics']
```

## Add metrics

You can add additional metrics directly to the ReportCollector class, but the recommended
method would be to create a new file, e.g. devices_metrics.py and define a new Collector
subclass.

To enable collection, import the new collector in the __init__.py file, under ReportMetricConfig.ready and register it with the collector registry.

Example:
```
    ...
    def ready(self):
        super().ready()
        from .report_metrics import ReportCollector
        from .device_metrics import DeviceCollector
        REGISTRY.register(ReportCollector())
        REGISTRY.register(DeviceCollector())
```

Imports of new collectors MUST be in the ready method of the plugig to ensure that all
Django models have been loaded and is ready for import. Attempting to import collectors
which may rely on Django/Netbox models being available outside of the ready method will result in a AppRegistryNotReady exception.