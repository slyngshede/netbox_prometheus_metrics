from extras.plugins import PluginConfig
from prometheus_client.core import REGISTRY


class ReportMetricConfig(PluginConfig):
    name = 'netbox_prometheus_metrics'
    verbose_name = 'Custom Prometheus Metrics'
    description = 'Adds additional metrics to Prometheus metrics endpoint'
    version = '0.1'
    author = 'Simon Lyngshede'
    author_email = 'slyngshede@wikimedia.org'
    middleware = []
    default_settings = {}

    def ready(self):
        super().ready()
        from .report_metrics import ReportCollector
        from .device_metrics import DeviceCollector
        REGISTRY.register(ReportCollector())
        REGISTRY.register(DeviceCollector())


config = ReportMetricConfig
