from collections import Counter
from typing import Iterable

from dcim.models import Device

from prometheus_client.metrics_core import Metric
from prometheus_client.core import GaugeMetricFamily, Gauge
from prometheus_client.registry import Collector


class DeviceCollector(Collector):
    def collect(self) -> Iterable[Metric]:
        """Generate metrics for generated reports, using job data.

        Yields:
            Iterator[Iterable[Metric]]: Generated metrics
        """

        gauge = GaugeMetricFamily('netbox_device_count', 'number of devices in netbox', labels=['status', 'datacenter', 'rackgroup', 'manufacturer'])
        counter = Counter()
        for device in Device.objects.all().values_list(
            "status", "site__slug", "rack__location__slug", "device_type__manufacturer__slug"
        ):
            print(device)
            counter[(device[0], device[1], device[2], device[3])] += 1

        for params, count in counter.items():
            gauge.add_metric(params, count)

        yield gauge

