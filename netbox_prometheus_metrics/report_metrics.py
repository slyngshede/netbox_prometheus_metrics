from typing import Iterable
from extras import reports
from prometheus_client.metrics_core import Metric

COMPAT = False
try:
    from extras.reports import ReportModule
except ImportError:
    COMPAT = True


from prometheus_client.core import GaugeMetricFamily
from prometheus_client.registry import Collector


class ReportCollector(Collector):
    def compat(self) -> Iterable[Metric]:
        """Older versions of Netbox has a get_reports method,
        which can be used to more easily get access to reports,
        but with less detail.

        Yields:
            Iterator[Iterable[Metric]]: Generated metrics
        """
        for module in reports.get_reports():
            module_name, report_set = module
            if not report_set:
                continue

            for report in report_set:
                metric_name = f'netbox_report_{report.module}_{report.name.lower()}'
                yield GaugeMetricFamily(metric_name,
                                        'Report generation failed',
                                        value=report.failed)

    def metrics(self) -> Iterable[Metric]:
        """Generate metrics for generated reports, using job data.

        Yields:
            Iterator[Iterable[Metric]]: Generated metrics
        """
        for module in ReportModule.objects.all():
            # Module name is always return as the file name, containing the
            # report, this means that it could contain a .py extention.
            module_name = module.name.rstrip('.py')

            # Each module may contain zero or more reports.
            for _, report in module.reports.items():

                # Find the report status by checking the status of the last
                # run background job. Order by creation date, decending.
                job = module.jobs.filter(
                    name=report.class_name).order_by('-created').first()

                if not job:
                    continue

                failed = 1 if job.status == 'errored' else 0
                duration = job.completed - job.started

                # Prometheus metrics name
                metric_name = f'netbox_report_{module_name}_\
{report.class_name.lower()}'

                yield GaugeMetricFamily(
                    metric_name,
                    'Report generation failed',
                    value=failed
                )

                yield GaugeMetricFamily(
                    f'{metric_name}_duration',
                    'Number of seconds for report generation',
                    value=duration.seconds
                )

    def collect(self) -> Iterable[Metric]:
        """Generate Prometheus metrics for Netbox report generation.

        Yields:
            Iterator[Iterable[Metric]]: Prometheus Metrics
        """

        # Check if this Netbox installation uses the old get_reports,
        # or if we need to get data from the background worker.
        if COMPAT:
            yield from self.compat()
        else:
            yield from self.metrics()
